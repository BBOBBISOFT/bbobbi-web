const NavBar = () => {
  return (
    <span>
      <ul>
        <li>
          <a href="/">Home</a>
        </li>
        <li>
          <a href="Projects">Projects</a>
        </li>
        <li>
          <a href="https://github.com/BBOBBISOFT">Github</a>
        </li>
      </ul>
    </span>
  );
};

export default NavBar;
