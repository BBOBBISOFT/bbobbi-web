import Head from "next/head";
import styles from "../styles/Projects.module.css";

export default function Projects() {
  return (
    <div className={styles.all}>
      <Head>
        <title>Projects</title>
        <meta name="description" content="BBOBBISOFT OFFICIAL" />
      </Head>
      <main>
        <div className={styles.title}>
          <h1>Plugins</h1>
          <p>BBOBBISOFT Minecraft Plugins</p>
        </div>
        <div className={styles.items}>
          <section className={styles.projects}>
            <h3>Knvil</h3>
            <p>Knvil은 BBOBBISOFT가 개발한 첫 Kotlin 기반 플러그인 입니다.</p>
          </section>
          <section className={styles.projects}>
            <h3>Jnvil</h3>
            <p>
              Jnvil은 BBOBBISOFT가 개발한 Java언어 기능 테스트용 플러그인
              입니다.
            </p>
          </section>
          <section className={styles.projects}>
            <h3>KnvilNext</h3>
            <p>
              KnvilNext는 BBOBBISOFT가 개발한 1.17.1 버전 타겟 최신 플러그인
              입니다.
            </p>
          </section>
        </div>
      </main>
    </div>
  );
}
